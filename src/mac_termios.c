/*
 * This file is part of the libserialport project.
 *
 * Copyright (C) 2013 Martin Ling <martin-libserialport@earth.li>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * At the time of writing, glibc does not support the Linux kernel interfaces
 * for setting non-standard baud rates and flow control. We therefore have to
 * prepare the correct ioctls ourselves, for which we need the declarations in
 * linux/termios.h.
 *
 * We can't include linux/termios.h in serialport.c however, because its
 * contents conflict with the termios.h provided by glibc. So this file exists
 * to isolate the bits of code which use the kernel termios declarations.
 *
 * The details vary by architecture. Some architectures have c_ispeed/c_ospeed
 * in struct termios, accessed with TCSETS/TCGETS. Others have these fields in
 * struct termios2, accessed with TCSETS2/TCGETS2. Some architectures have the
 * TCSETX/TCGETX ioctls used with struct termiox, others do not.
 */

#include <config.h>
#include <stdlib.h>
#include "termios.h"
#include "mac_termios.h"

SP_PRIV unsigned long get_termios_get_ioctl(void)
{
    return TIOCGETA;
}

SP_PRIV unsigned long get_termios_set_ioctl(void)
{
    return TIOCSETA;
}

SP_PRIV size_t get_termios_size(void)
{
	return sizeof(struct termios);
}

#if (defined(HAVE_TERMIOS_SPEED) || defined(HAVE_TERMIOS2_SPEED)) && HAVE_DECL_BOTHER
SP_PRIV int get_termios_speed(void *data)
{
    struct  termios * term = (struct termios *) data;
    return (int) cfgetispeed(term);  // use cfgetispeed or cfgetospeed ???
}

SP_PRIV void set_termios_speed(void *data, int speed)
{
    struct  termios * term = (struct termios *) data;
    cfsetspeed(term, speed);
    // need to call tcsetattr ???

}
#endif

#ifdef HAVE_STRUCT_TERMIOX
SP_PRIV size_t get_termiox_size(void)
{
	return sizeof(struct termiox);
}

SP_PRIV int get_termiox_flow(void *data, int *rts, int *cts, int *dtr, int *dsr)
{
	struct termiox *termx = (struct termiox *) data;
	int flags = 0;

	*rts = (termx->x_cflag & RTSXOFF);
	*cts = (termx->x_cflag & CTSXON);
	*dtr = (termx->x_cflag & DTRXOFF);
	*dsr = (termx->x_cflag & DSRXON);

	return flags;
}

SP_PRIV void set_termiox_flow(void *data, int rts, int cts, int dtr, int dsr)
{
	struct termiox *termx = (struct termiox *) data;

	termx->x_cflag &= ~(RTSXOFF | CTSXON | DTRXOFF | DSRXON);

	if (rts)
		termx->x_cflag |= RTSXOFF;
	if (cts)
		termx->x_cflag |= CTSXON;
	if (dtr)
		termx->x_cflag |= DTRXOFF;
	if (dsr)
		termx->x_cflag |= DSRXON;
}
#endif
