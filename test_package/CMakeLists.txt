cmake_minimum_required(VERSION 3.1)
project(test_package)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

if(APPLE)
    find_library(CORE_FOUNDATION CoreFoundation)
    if(NOT CORE_FOUNDATION)
        message(FATAL_ERROR "Apple Core Foundation framework not found!")
    endif()
    find_library(IOKIT_LIBRARY IOKit)
    if(NOT IOKIT_LIBRARY)
        message(FATAL_ERROR "Apple Core IOKit framework not found!")
    endif()
endif()

if(MSVC)
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
    set(WINDOWS_LIB "setupapi")
endif()

add_executable(${PROJECT_NAME} test_package.cpp)
target_link_libraries(${PROJECT_NAME} ${CONAN_LIBS} ${CORE_FOUNDATION} ${IOKIT_LIBRARY} ${WINDOWS_LIB})
