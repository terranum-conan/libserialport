# libserialport

Cross-platform library for serial port communication and enumeration. Based on :

 - Sigrok's libserialport (https://sigrok.org/wiki/Libserialport)
 - https://github.com/scottmudge/libserialport-cmake (port of the library using cmake build system)

## Create conan package

### One step package creation

        conan create . terranum-conan+libserialport/stable

### Upload package to gitlab

      conan upload libserialport/0.1.1-beta@terranum-conan+libserialport/stable --remote=gitlab -q 'build_type=Release'

### Build the debug package

      conan create . terranum-conan+libserialport/stable -s build_type=Debug

Don't upload debug package on Gitlab.

### Step by step package creation

1. Get source code

        conan source . --source-folder=_bin/source
2. Create install files

        conan install . --install-folder=_bin/build
3. Build

        conan build . --source-folder=_bin/source --build-folder=_bin/build
4. Create package

        conan package . --source-folder=_bin/source --build-folder=_bin/build --package-folder=_bin/package
5. Export package

        conan export-pkg . terranum-conan+libserialport/stable --source-folder=_bin/source --build-folder=_bin/build
6. Test package

        conan test test_package libserialport/0.1.1-beta@terranum-conan+libserialport/stable