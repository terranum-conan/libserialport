from conans import ConanFile, CMake, tools, AutoToolsBuildEnvironment
import os
import subprocess


class serialport(ConanFile):
    name = "libserialport"
    description = "libserialport"
    topics = ("conan", "libserialport", "serial")
    url = "https://github.com/sigrokproject/libserialport"
    homepage = "https://github.com/sigrokproject/libserialport"
    license = "GPLv2"
    settings = "os", "arch", "compiler", "build_type"
    version = "0.1.1-beta"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"
    exports_sources = {"src/*", "CMakeLists.txt"}

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="src")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)





